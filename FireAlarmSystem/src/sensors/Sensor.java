package sensors;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import javax.swing.Timer;

public class Sensor {

	ObjectOutputStream sensorWrite; // This object is used for writing in socket 
	BufferedReader sensorRead;// This object is used for reading in socket.
	String sensorType;// Type of the sensor
	String sensorId;//Sensor ID which contains floor and alarm number
	String serverAddress;//Server address(ex:localhost)
	Socket socket;//initialize socket variable
	double[] sensorReadingArr; //Array to be used in communication
	int arrayIndex;//variable to be used in arrays
	static HashSet<String> sensorList = new HashSet<String>(); // This is a hashSet with all available sensor types

	//constructor;the parameters will be taken from the user input which are taken through main method
	public Sensor(String senType, String sid, String server) { 
		this.sensorType = senType;
		this.sensorId = sid;
		this.serverAddress = server;
	}
	
	public void run() throws Exception {
		try {
			socket = new Socket(this.serverAddress, 9008);//server port is taken as 9008
			sensorRead = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			sensorWrite = new ObjectOutputStream(socket.getOutputStream());
		} catch (Exception e) {
			System.out.println("Error in sensor");
			return;
		}

		while (true) {
			String serverMessage = sensorRead.readLine();

			if (serverMessage.startsWith("SENSORDETAILS")) {
				String details = this.sensorId + "--" + this.sensorType;
				sensorWrite.writeObject(details);

			} else if (serverMessage.startsWith("SENSORADDED")) {
				System.out.println("Connection successful \n");
				sensorReadingArr = new double[12];
				arrayIndex = -1;
				break;
				
			} else if (serverMessage.startsWith("ALREADYTAKEN")) {
				System.out.println("This sensor has been already added");
				return;
			}
		}

		while (true) {
			sleepTime.start();
			sendServerTimer.start();
		}
	}

	Timer sleepTime = new Timer(1000, new ActionListener() { // to make it easy timer has been set to 1000,it shud be 5
																// seconds
		@Override
		public void actionPerformed(ActionEvent e) {		
			arrayIndex++;
			sensorReadingArr[arrayIndex] = getReading(sensorType);
		}
	});

	Timer sendServerTimer = new Timer(12000, new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent e) {

			synchronized (sensorReadingArr) {

				try {
					sensorWrite.writeUTF("NEWVALUEREADING");// Indicating that new reading is going to be send to the
															// server.
					sensorWrite.writeObject(sensorReadingArr);	
				}

				catch (Exception ex) {

				}
				sensorReadingArr = new double[13];
				arrayIndex = -1;
			}
			System.out.println("Updated information for this hour \n\n");
		}
	});

	public double getReading(String type) {
		Random rand = new Random();
		double newReading=0;
		if(type.equals("temperature")) {
			newReading=rand.nextInt(100); // upper limit for the random number will be 100
		}
		else if(type.equals("battery")) {
			newReading=rand.nextInt(100);
		}
		else if(type.equals("smoke")) {
			newReading=rand.nextInt(10);
		}
		else if(type.equals("CO2")) {
			newReading=rand.nextInt(305-295)+295;//to make the random number range between 295-305
		}
		System.out.println(newReading);
		System.out.println(type);
		return newReading;
	}

	
	
	public static void main(String[] args) throws Exception {
		// adding the current sensor types to the sensorList.
		sensorList.add("temperature");
		sensorList.add("battery");
		sensorList.add("smoke");
		sensorList.add("CO2");

		String type = "";
		boolean checked = false;

		while (!checked) {
			System.out.print("Enter sensor type(temperature/battery/smoke/CO2) : ");
			Scanner sc1 = new Scanner(System.in);
			type = sc1.nextLine();
			if (sensorList.contains(type))
				checked = true;
			else
				System.out.println("Please make sure to enter valid sensor type");
		}

		System.out.print("Enter floor number of sensor : ");
		Scanner sc2 = new Scanner(System.in);
		int floorNum = sc2.nextInt();

		System.out.print("Enter sensor number of floor " + floorNum + " : ");
		Scanner sc3 = new Scanner(System.in);
		String sensorID = floorNum + "-" + sc3.nextInt();
		// System.out.println(sensorID);
	
		System.out.print("Enter server address : ");
		Scanner sc4 = new Scanner(System.in);
		String serveraddress = sc4.nextLine();

		// After all the inputs are entered, new sensor object is created
		Sensor sensor = new Sensor(type, sensorID, serveraddress);
		sensor.run();
	}

}
