package server;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.swing.Timer;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import interfaces.AlarmListener;
import interfaces.AlarmServer;

public class sensorServer {

	private static final int PORT = 9008;
	
	private static HashMap<String, PrintWriter> sensorWriters = new HashMap<String, PrintWriter>();
	//Stores the sensors' names with their  printWriter
	private static HashSet<String> SensorNames = new HashSet<String>();
	// Stores the sensor names
	private static HashMap<String, AlarmListener> listeners = new HashMap<String, AlarmListener>(); 
	// Stores the monitoring listener instances along with their id's
	
	AlarmListener monitor;

	public static void main(String[] args) throws Exception {
		System.out.println("Server is running ...");
		ServerSocket listener = new ServerSocket(PORT);
		try {
			// Creating the RMI registry and binding the server as AlarmServer to monitor the stations
			Registry reg = LocateRegistry.createRegistry(9011); //port 9011 was used to make it an uncommon port
			reg.rebind("AlarmServer", new AlarmService());
			while (true) {
				new SensorHandler(listener.accept()).start();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			listener.close();
		}
	}

	private static class SensorHandler extends Thread {

		private Socket socket;
		private PrintWriter serverOutput;
		private String sensorName;// sensor name = sensor ID + sensor type
		private File file;
		private FileWriter fw;
		private BufferedWriter bw;
		private PrintWriter pWriter;
		private ObjectInputStream serverInput;

		public SensorHandler(Socket soc) {
			this.socket = soc;
		}

		public void run() {
			try {

				serverInput = new ObjectInputStream(socket.getInputStream());
				serverOutput = new PrintWriter(socket.getOutputStream(), true);

				while (true) {
					serverOutput.println("SENSORDETAILS");
					sensorName = String.valueOf(serverInput.readObject());
					if (sensorName == null) {
						return;
					}

					synchronized (sensorName) {

						if (!SensorNames.contains(sensorName)) {
							SensorNames.add(sensorName);
							sensorWriters.put(sensorName, serverOutput);
							sensorTracker.start();
							break;
						} else {
							serverOutput.println("ALREADYTAKEN");
						}
					}
				}
				serverOutput.println("SENSORADDED");

				while (true) {
					String line = serverInput.readUTF();
					if (line.startsWith("NEWVALUEREADING")) {
						sensorTracker.stop();
						double[] arr = (double[]) serverInput.readObject();
						sensorReport(arr);
						processDetails(sensorName, arr);
						sensorTracker.start();
					}
				}

			} catch (Exception e) {

			} finally {

				if (sensorName != null) {
					sensorWriters.remove(sensorName);
					SensorNames.remove(sensorName);
				}
				try {
					socket.close();
				} catch (Exception e) {
				}
			}
		}

		Timer sensorTracker = new Timer(12000, new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					if (!SensorNames.contains(sensorName)) {
						String parts[] = sensorName.split("--");
						for (Map.Entry<String, AlarmListener> entry : listeners.entrySet()) {
							entry.getValue().sensorFailAlert(parts[0], parts[1]);
						}
					} else {
						stopTracking();
					}

				} catch (Exception ex) {
				}
			}
		});

		public void stopTracking() {
			sensorTracker.stop();
		}

		//This method will check whether smoke level will be increaded more than 7.0 and whether temperature level will be greater than 50
		//IF so this will alert the monitor
		public void processDetails(String sensrName, double[] arr) throws Exception {
			String[] parts = sensrName.split("--");
			if (parts[1].equals("smoke")) {
				if (getAverage(arr) > 7.0) {
					for (Map.Entry<String, AlarmListener> entry : listeners.entrySet()) {
						entry.getValue().dangerAlert("Smoke level is critical at " + parts[0]);
					}
				}
			} else if (parts[1].equals("temperature")) {
				if (getAverage(arr) > 50.0) {
					for (Map.Entry<String, AlarmListener> entry : listeners.entrySet()) {
						entry.getValue().dangerAlert("Temperature is above 50 Celcius at " + parts[0]);
					}
				}
			}
		}

		public double getAverage(double[] arr) {
			double sum = 0;
			for (double a : arr)
				sum += a;

			return sum / arr.length;
		}

		//This is used to be updated when new data for a specific sensor is added
		public void sensorReport(double[] arr) throws Exception {

			try {
				file = new File(sensorName + ".txt");

				if (file.exists()) {
					file.delete();
				}
				file.createNewFile();

				fw = new FileWriter(file.getAbsolutePath());
				bw = new BufferedWriter(fw);
				pWriter = new PrintWriter(bw);
				fw.flush();
				bw.flush();
				pWriter.flush();
				for (double d : arr) {
					pWriter.flush();
					pWriter.println(d);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			fw.flush();
			bw.flush();
			pWriter.flush();
		}
	}

	private static class AlarmService extends UnicastRemoteObject implements Runnable, AlarmServer {
		String[] sensorTypes = { "temperature", "battery", "smoke", "CO2" };

		public AlarmService() throws Exception {

		}

		public boolean newListener(String id, AlarmListener listener) throws java.rmi.RemoteException {
			if (!listeners.containsKey(id)) {
				synchronized (listeners) {
					listeners.put(id, listener);
				}
				System.out.println("monitor added");
				return true;
			} else {
				return false;
			}
		}

		@Override
		public void run() {

		}

		@Override
		public int getMonitorNum() throws RemoteException {
			return listeners.size()-1;
		}

		@Override
		public int getSensorCount() throws RemoteException {
			return SensorNames.size();
		}

		@Override
		public HashMap<String, Double> getLatestFireStatics(String id) throws RemoteException {
			BufferedReader br;
			FileReader fr;
			double data = 0;
			int lineCount = 0;
			HashMap<String, Double> fireMap = new HashMap<String, Double>();

			for (String sen : sensorTypes) {
				String fileName = id + "--" + sen + ".txt";
				String sensorName = id + "--" + sen;
				String line;
				if (SensorNames.contains(sensorName)) {
					try {
						fr = new FileReader(fileName);
						br = new BufferedReader(fr);

						while ((line = br.readLine()) != null) {
							data += Double.parseDouble(line);
							lineCount++;
						}

					} catch (Exception e) {
					}
				} else {
					data = 0;
					lineCount = 1;
				}

				fireMap.put(sen, data / lineCount);
				data = 0;
				lineCount = 0;
			}

			return fireMap;
		}

		@Override
		public void removeListener(String id) throws RemoteException {
			listeners.remove(id);
			System.out.println("monitor removed");
		}


		@Override
		public String getSensorIDlist() throws RemoteException {
				String idd="";
			
			for (String sen : SensorNames) {
				String[] parts = sen.split("--");
				//idd=idd+parts[0]+"/";
				idd=idd.concat(parts[0]+"/");
			}
		
			return idd;
		}

		//This is to get the latest reading for a given sensor id
		@Override
		public double getCurrentReading(String sensorName) throws RemoteException {
			String fileName = sensorName + ".txt";
			BufferedReader br;
			FileReader fr;
			double reading = 0.0;
			String line = "";
			int linecount = 0;
			try {
				fr = new FileReader(fileName);
				br = new BufferedReader(fr);
				reading = Double.parseDouble(br.readLine());
				reading=reading/1.0;

			} catch (Exception e) {
			}
			return reading;
		}
	}
}
