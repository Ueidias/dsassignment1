package interfaces;

import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.HashSet;

public interface AlarmServer extends java.rmi.Remote{

		//To add new monitors
		public boolean newListener(String id, AlarmListener listener) throws java.rmi.RemoteException;
		//To remove monitors
		public void removeListener(String id) throws java.rmi.RemoteException;
		//To get the latest reading of a certain sensor
		public double getCurrentReading(String sensorName) throws java.rmi.RemoteException;
		//To get the count of monitors
	    public int getMonitorNum() throws java.rmi.RemoteException;
	    //To get count of sensors
	    public int getSensorCount() throws java.rmi.RemoteException;
	    //To get latest statistics averages of sensors in particular id
	    public HashMap<String,Double> getLatestFireStatics(String id) throws java.rmi.RemoteException;
	    //To get the working sensor ID list
	    public String getSensorIDlist() throws RemoteException;
	   
}
