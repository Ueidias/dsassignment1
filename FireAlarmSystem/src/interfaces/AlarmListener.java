package interfaces;

public interface AlarmListener extends java.rmi.Remote {

	//This method is to notify when a sensor goes down
	 public void sensorFailAlert(String id, String sensorType) throws java.rmi.RemoteException;
	 //This method is to alert when a sensor is in danger
	 public void dangerAlert(String message) throws java.rmi.RemoteException;
	
}
