package clients;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileReader;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.AbstractButton;
import javax.swing.AbstractListModel;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JSeparator;
import java.awt.TextField;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.TextArea;
import java.awt.List;
import java.awt.ScrollPane;
import javax.swing.JScrollPane;
import javax.swing.JMenu;
import java.awt.event.TextListener;
import java.awt.event.TextEvent;

public class AlarmMonitor extends JFrame {

	static clientHandler handler;
	static String iD;
	static DefaultComboBoxModel model;
	Thread t;
	static AlarmMonitor panel;
	JButton blogout;
	JPanel loginpanel;
	JLabel id;
	JLabel lblsensorCount;
	private JLabel lblSensorName;
	private JLabel lblCurrentReading;
	private static JComboBox cmbSensors;
	private static TextField txtTemp;
	private static TextField txtBattery;
	private static TextField txtSmoak;
	private static TextField txtCO2;
	private static TextField senCount;
	private static TextField monitorCnt;
	private static TextField txtCurrentReading;
	private static TextField txtID;
	private static TextField textField;
	private static TextArea textArea;
	private JLabel lblCurrentIdList;

	public AlarmMonitor() {
		super("Alarm Monitor");
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					handler.removeListener(iD);

				} catch (Exception ee) {
				}
			}
		});
		setTitle("Alarm Monitor Dashboard");

		blogout = new JButton("Logout");
		loginpanel = new JPanel();
		id = new JLabel("ID");
		lblsensorCount = new JLabel("Total Sensor count");
		
		TextField txtCurrentReading = new TextField();
		txtCurrentReading.setEditable(false);
		txtCurrentReading.setBounds(509, 140, 102, 22);
		loginpanel.add(txtCurrentReading);

		TextField txtID = new TextField();
		txtID.setText("0-0");
		txtID.setBounds(118, 26, 102, 22);
		loginpanel.add(txtID);
		setSize(655, 495);
		setLocation(500, 280);
		loginpanel.setLayout(null);
		blogout.setBounds(549, 11, 80, 20);
		id.setBounds(45, 26, 38, 20);
		lblsensorCount.setBounds(345, 337, 138, 20);

		JComboBox cmbSensors_1 = new JComboBox();
		cmbSensors_1.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				
				try {
					System.out.println(String.valueOf(txtID.getText() + "--" + cmbSensors_1.getSelectedItem()));
					txtCurrentReading.setText(new DecimalFormat("#0.0").format(handler.getCurrentReading(
							String.valueOf(txtID.getText() + "--" + cmbSensors_1.getSelectedItem()))));
					//System.out.println("*************************");
				} catch (Exception ee) {
					System.out.println(ee.getMessage());
				}
			}
		});
		cmbSensors_1.setModel(new DefaultComboBoxModel(new String[] { "Temperature", "Battery", "Smoke", "CO2" }));
		cmbSensors_1.setBounds(396, 109, 102, 20);
		loginpanel.add(cmbSensors_1);

		loginpanel.add(blogout);
		loginpanel.add(id);
		loginpanel.add(lblsensorCount);

		getContentPane().add(loginpanel);

		JLabel lblMonitorCount = new JLabel("Total Monitor count");
		lblMonitorCount.setBounds(345, 386, 118, 20);
		loginpanel.add(lblMonitorCount);

		JLabel lblTemperature = new JLabel("Temperature(C)");
		lblTemperature.setBounds(20, 95, 102, 20);
		loginpanel.add(lblTemperature);

		JLabel lblBatteryLevel = new JLabel("Battery Level(%)");
		lblBatteryLevel.setBounds(20, 140, 102, 20);
		loginpanel.add(lblBatteryLevel);

		JLabel lblSmokeLevel = new JLabel("Smoke level(0-10)");
		lblSmokeLevel.setBounds(20, 181, 102, 20);
		loginpanel.add(lblSmokeLevel);

		JLabel lblCoLevel = new JLabel("Co2 level(ppm)");
		lblCoLevel.setBounds(20, 224, 102, 20);
		loginpanel.add(lblCoLevel);

		lblSensorName = new JLabel("Sensor name");
		lblSensorName.setBounds(268, 109, 80, 20);
		loginpanel.add(lblSensorName);

		lblCurrentReading = new JLabel("Current Reading");
		lblCurrentReading.setBounds(268, 140, 143, 20);
		loginpanel.add(lblCurrentReading);

		txtID.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				iD = (String) txtID.getText();
				System.out.println(iD);

				String format = "#0.0";
				try {

					for (Map.Entry<String, Double> entry : handler.getLatestFireStatics(iD).entrySet()) {
						if (entry.getValue() == 0)
							format = "#0.00";
						else
							format = "#0.0";

						switch (entry.getKey()) {
						case "temperature":
							txtTemp.setText(new DecimalFormat(format).format(entry.getValue()));
							break;
						case "battery":
							txtBattery.setText(new DecimalFormat(format).format(entry.getValue()));
							break;
						case "smoke":
							txtSmoak.setText(new DecimalFormat(format).format(entry.getValue()));
							break;
						case "CO2":
							txtCO2.setText(new DecimalFormat(format).format(entry.getValue()));
							break;
						}
					}
				} catch (Exception ex) {
				}
			}

		});

		JSeparator separator = new JSeparator();
		separator.setBounds(286, 298, 343, 7);
		loginpanel.add(separator);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		Writer writer = null;

		blogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					handler.removeListener(iD);
					dispose();
					login l = new login();
					l.setVisible(true);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});

		iD = "5-6";
		model = new DefaultComboBoxModel();

		try {
			handler = new clientHandler(iD, this);
			System.out.println(handler.connectToServer());
		} catch (Exception e) {
		}

		txtTemp = new TextField();
		txtTemp.setEditable(false);
		txtTemp.setBounds(141, 95, 102, 22);
		loginpanel.add(txtTemp);

		txtBattery = new TextField();
		txtBattery.setEditable(false);
		txtBattery.setBounds(141, 140, 102, 22);
		loginpanel.add(txtBattery);

		txtCO2 = new TextField();
		txtCO2.setEditable(false);
		txtCO2.setBounds(141, 224, 102, 22);
		loginpanel.add(txtCO2);

		txtSmoak = new TextField();
		txtSmoak.setEditable(false);
		txtSmoak.setBounds(141, 181, 102, 22);
		loginpanel.add(txtSmoak);

		senCount = new TextField();
		senCount.setEditable(false);
		senCount.setBounds(509, 335, 102, 22);
		loginpanel.add(senCount);

		monitorCnt = new TextField();
		monitorCnt.setEditable(false);
		monitorCnt.setBounds(509, 384, 102, 22);
		loginpanel.add(monitorCnt);

		TextArea textArea = new TextArea();
		textArea.setBounds(140, 316, 80, 112);

		textArea.setText("ID list \n");
		loginpanel.add(textArea);

		textField = new TextField();
		textField.addTextListener(new TextListener() {
			public void textValueChanged(TextEvent e) {
				if ((!textArea.getText().contains(textField.getText()))&&(textField.getText()!=null))
				{	
					textArea.append(textField.getText() + "\n");
				}
			}
		});
		textField.setEditable(false);
		textField.setBounds(141, 268, 102, 22);
		textField.setVisible(false);
		loginpanel.add(textField);

		lblCurrentIdList = new JLabel("Current ID list");
		lblCurrentIdList.setBounds(20, 314, 80, 20);
		loginpanel.add(lblCurrentIdList);

	}

	public AlarmMonitor(String id, String serverIp, int port) {

		this();
		iD = id;
		model = new DefaultComboBoxModel();
		TextArea textArea = new TextArea();

		try {
			handler = new clientHandler(iD, this);
			System.out.println(handler.connectToServer(serverIp, port));
		} catch (Exception e) {
		}

		t = new Thread(new Updater());
		t.start();

	}

	public static void main(String args[]) {
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(AlarmMonitor.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(AlarmMonitor.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(AlarmMonitor.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(AlarmMonitor.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		}
		// </editor-fold>
		System.out.println("triggered");
		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
						| UnsupportedLookAndFeelException e) {
					e.printStackTrace();
				}
				new AlarmMonitor().setVisible(true);
			}
		});

		Updater updater = new Updater();
		updater.run();
	}

	public static class Updater implements Runnable {

		Timer refreshMonitor = new Timer(5000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String format = "#0.0";
				try {

					for (Map.Entry<String, Double> entry : handler.getLatestFireStatics(iD).entrySet()) {
						if (entry.getValue() == 0)
							format = "#0.00";
						else
							format = "#0.0";

						switch (entry.getKey()) {
						case "temperature":
							txtTemp.setText(new DecimalFormat(format).format(entry.getValue()));
							break;
						case "battery":
							txtBattery.setText(new DecimalFormat(format).format(entry.getValue()));
							break;
						case "smoke":
							txtSmoak.setText(new DecimalFormat(format).format(entry.getValue()));
							break;
						case "CO2":
							txtCO2.setText(new DecimalFormat(format).format(entry.getValue()));
							break;
						}
					}
				} catch (Exception ex) {
				}
			}
		});

		public void run() {

			while (true) {
				try {
					senCount.setText(String.valueOf(handler.getSensorCount()));
					monitorCnt.setText(String.valueOf(handler.getMonitorNum()));
					refreshMonitor.start();
					 String sss = handler.getIDs();
					// System.out.println(sss);
					 String[] result = sss.split("/");
					 
					 for (int x=0; x<result.length; x++) {
					 
						 	textField.setText(null);
					 		//System.out.println(result[x]);
					 		textField.setText(result[x]);
					 		Thread.sleep(5000);
								 		
					 }
				} catch (Exception e) {
					//System.out.println(e.getMessage());
				}
			}
		}
	}
}
