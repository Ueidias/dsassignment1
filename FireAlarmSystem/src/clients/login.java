package clients;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.security.MessageDigest;
import java.util.*;

public class login extends JFrame {
	JButton blogin;
	JPanel loginpanel;
	JTextField txuser;
	JTextField pass;
	JLabel username;
	JLabel password;
	private JTextField senIDtxt;

	public login() {
		super("User login");

		blogin = new JButton("Login");
		loginpanel = new JPanel();
		txuser = new JTextField(15);
		pass = new JPasswordField(15);
		username = new JLabel("Username");
		password = new JLabel("Password");

		setSize(450, 205);
		setLocation(500, 280);
		loginpanel.setLayout(null);

		txuser.setBounds(115, 28, 150, 20);
		pass.setBounds(115, 63, 150, 20);
		blogin.setBounds(315, 127, 80, 20);
		username.setBounds(20, 28, 80, 20);
		password.setBounds(20, 63, 80, 20);

		loginpanel.add(blogin);
		loginpanel.add(txuser);
		loginpanel.add(pass);
		loginpanel.add(username);
		loginpanel.add(password);

		getContentPane().add(loginpanel);

		JLabel lblAllocatedSenser = new JLabel("Allocated sensor ID(floor No. - sensor No. )");
		lblAllocatedSenser.setBounds(20, 96, 257, 20);
		loginpanel.add(lblAllocatedSenser);

		senIDtxt = new JTextField(15);
		senIDtxt.setBounds(351, 96, 44, 20);
		loginpanel.add(senIDtxt);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);

		Writer writer = null;

		blogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					FileReader file = new FileReader("loginCredentials.txt");//Read the credentials from this file as this system do not have a database
					Scanner scan = new Scanner(file);
					;
					String line = null;

					String usertxt = " ";
					String passtxt = " ";
					String puname = txuser.getText();
					String ppaswd = pass.getText();
					String sID = senIDtxt.getText();
					ppaswd=getencrypted(ppaswd);
					
					while (scan.hasNext()) {
						usertxt = scan.nextLine();
						passtxt = scan.nextLine();
					}

					if (puname.equals(usertxt) && ppaswd.equals(passtxt) && (!sID.equals(""))) {
						// AlarmMonitor menu =new AlarmMonitor("5-6","127.0.0.1",9011);
						AlarmMonitor menu = new AlarmMonitor(sID, "127.0.0.1", 9011);//assumption that one monitor is allocated to a specific sensor
						dispose();
						menu.setVisible(true);
					} else if (puname.equals("") && ppaswd.equals("")) {
						JOptionPane.showMessageDialog(null, "Please insert Username , Password and relevant ID");
					} else {

						JOptionPane.showMessageDialog(null, "Wrong Username / Password / ID");
						txuser.setText("");
						pass.setText("");
						txuser.requestFocus();
					}
				} catch (IOException d) {
					d.printStackTrace();
				}
			}
		});
	}

	public static void main(String args[]) {
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new login().setVisible(true);
			}
		});
	}
	
	//To encrypt the password
    public String getencrypted(String password){
        StringBuffer stringb = new StringBuffer();
        try{
            MessageDigest messageD = MessageDigest.getInstance("MD5");
            messageD.update(password.getBytes());

            byte byteData[] = messageD.digest();
            for (int i = 0; i < byteData.length; i++) {
            	stringb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }
        }
        catch (Exception e){

        }
        //System.out.println(sb.toString());
        return stringb.toString();
    }
}