package clients;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import interfaces.AlarmListener;
import interfaces.AlarmServer;

public class clientHandler extends UnicastRemoteObject implements AlarmListener{

	 private String id;
	    AlarmServer server;
	    JFrame monitor;
	    
	    public clientHandler(String iD, JFrame wm) throws Exception{
	        this.id = iD;
	        this.monitor = wm;
	    }
	    
	    public boolean connectToServer(){
	        
	        try {
	            //RMI connection
	            Registry reg = LocateRegistry.getRegistry("127.0.0.1", 9011);
	            server = (AlarmServer) reg.lookup("AlarmServer");
	            if(server.newListener(this.id,this)){
	                return true;
	            }
	        } 
	        catch (Exception ex) {
	            
	        }
	        return false;
	    }
	    
	    public boolean connectToServer(String ipAddress,int port){
	        
	        try {
	            
	            Registry reg = LocateRegistry.getRegistry(ipAddress, port);
	            server = (AlarmServer) reg.lookup("AlarmServer");
	            if(server.newListener(this.id,this)){
	                return true;
	            }
	        } 
	        catch (Exception ex) {
	            
	        }
	        return false;
	    }
	    	
	    public int getSensorCount() throws Exception{
	        return server.getSensorCount();
	    }
	    	    
	    public int getMonitorNum() throws Exception{
	        return server.getMonitorNum();
	    }
	    	   
	    public HashMap<String,Double> getLatestFireStatics(String id) throws Exception{
	        return server.getLatestFireStatics(id);
	    }
	    	    
	    public void removeListener(String iD) throws Exception{
	        server.removeListener(iD);
	    }
	    	  
	    public String getIDs() throws Exception{
	        return server.getSensorIDlist();
	    }

	    public void sensorFailAlert(String id, String sensorType) throws RemoteException {
	        String message = sensorType +" sensor at "+id+" has failed";
	        JOptionPane.showMessageDialog(monitor,message.toUpperCase(),"Warning",JOptionPane.WARNING_MESSAGE);
	    }
	   
	    public void dangerAlert(String message) throws RemoteException {
	        JOptionPane.showMessageDialog(monitor,message.toUpperCase(),"Warning",JOptionPane.WARNING_MESSAGE);
	    }
   	   
	    public double getCurrentReading(String sensorName) throws Exception{
	        return server.getCurrentReading(sensorName);
	    }
}
