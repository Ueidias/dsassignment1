#  This contains how to test the system.(---NETBEANS IDE---)
##  WARNING!:Do not delete loginCredentials.txt file
###  (loginCredentials.txt - Contains login credentials for the monitor)

1.Run the sensorServer.java in server package(Run as java application).

	1.1->console will print "Server is running ..." if everything is fine.

2.Run Sensor.java in sensors package(Run as java application).

	2.1.Enter sensor type(temperature/battery/smoke/CO2)->smoke
	2.2.Enter the floor number.(Enter a positive number)->2
	2.3.Enter the sensor number.(Enter a positive number)->5
	2.4.Enter the server address.->localhost
	-->"Connection successful" will be printed on console if everything fine.
	2.5.You can repeat the (step 2) when you want to add another sensor.
	
3.Run login.java in clients package(Run as java application).

	3.1.Enter username->admin
	3.2.Enter password->admin
	3.3.Enter the allocated sensor id->1-1
	-->Alarm monitor dashboard will pop up
	-->Currently working sensor id list is displayed in "Current ID List"
	-->Total sensor count and total monitor count is displayed at bottom right corner.
	-->Alert boxes may pop up when a sensor is failed or when there is a danger.
	
	3.4.Type the ID you want to search in the "ID" textbox->2-5
	->As I have only initialized smoke sensor for ID 2-5 above,smoke level textbox will give readings.
	3.5.Select item in "sensor name" textbox to get the current reading of that sensor(Make sure "ID" textbox is not empty).
	3.6.You can repeat the(step 3) to add another monitor to the system.
	3.7.Press "logout" button if you want to exit the alarm monitor dashboard. 
